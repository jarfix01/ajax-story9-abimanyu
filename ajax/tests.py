from django.test import TestCase,Client
from django.urls import resolve
from .views import index

# Create your tests here.

class UnitTest(TestCase):

    def test_main_url_exists(self):
        response = Client().get("/")
        self.assertEqual(response.status_code, 200)
    
    def test_return_main_html(self):
        response = Client().get("")
        self.assertTemplateUsed(response, "ajax/main.html")

    def test_main_view_func(self):
        found = resolve("/")
        self.assertEqual(found.func, index)
