from django.urls import path
from .views import index, JsonResponse

app_name = 'ajax'

urlpatterns = [
    path('', index),
    path('json/', JsonResponse)
]