import json

from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from .models import Data
from django.http import HttpResponse

# Create your views here.

def BookCheck(i,j):
    if not Data.objects.all()[i : j].exists():
        return ""
    else:
        book_dict = {
            "title"  : Data.objects.all()[i].title,
            "author" : Data.objects.all()[i].author,
            "likes"  : Data.objects.all()[i].likes,
        }
        return book_dict
    


@csrf_exempt
def index(request):
    
    if request.method == 'POST':
        title = request.POST["title"]
        author = request.POST["author[]"]

        if Data.objects.filter(title = title).exists():
            update_data = Data.objects.get(title = title)
            update_data.likes += 1
            update_data.save()
            print("Data Updated")
            print (update_data, update_data.likes)

        else:
            Data.objects.create(
                title = title,
                author = author,
                likes = 1
            )
            print("New Data Added")
            print(Data.objects.all())    


    
    return render(request, 'ajax/main1.html')


def JsonResponse(request):
    
    response_data = { 
        "items" : [
            {"book" : BookCheck(0, 1)},
            {"book" : BookCheck(1, 2)},
            {"book" : BookCheck(2, 3)},
            {"book" : BookCheck(3, 4)},
            {"book" : BookCheck(4, 5)}
        ]
    }

    return HttpResponse(json.dumps(response_data), content_type="application/json")